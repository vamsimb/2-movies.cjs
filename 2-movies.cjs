const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}

// NOTE: For all questions, the returned data must contain all the movie information including its name.



// Q1. Find all the movies with total earnings more than $500M. 

console.log("....................test1.....................")

const movieEarnedMoreThan500M = Object.entries(favouritesMovies).reduce((output, movie) => {
    if (movie[1].totalEarnings > "$500M") {
        output[movie[0]] = movie[1]
    }
    return output
}, {})

console.log(movieEarnedMoreThan500M);

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

console.log("....................test2.....................")

const movieEarnedMoreThan500MAnd3OscarNominations = Object.entries(favouritesMovies).reduce((output, movie) => {
    if (movie[1].totalEarnings > "$500M" && Number(movie[1].oscarNominations > 3)) {
        output[movie[0]] = movie[1]
    }
    return output
}, {})

console.log(movieEarnedMoreThan500MAnd3OscarNominations);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".

console.log("....................test3.....................")

const leonardoDicaprioMovies = Object.entries(favouritesMovies).reduce((output, movie) => {
    if (movie[1].actors.includes("Leonardo Dicaprio")) {
        output[movie[0]] = movie[1]
    }
    return output
}, {})

console.log(leonardoDicaprioMovies);

// Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.


console.log("....................test4.....................")

const sortedMoviesByIMDBRating = Object.fromEntries(Object.entries(favouritesMovies).sort(([, movie1], [, movie2]) => {

    if (movie1.imdbRating == movie2.imdbRating) {
        return movie1.totalEarnings - movie2.totalEarnings;
    }
    return movie1.imdbRating - movie2.imdbRating;
}))


console.log(sortedMoviesByIMDBRating);


// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime


// console.log("....................test5.....................")

// const movieGenres = Object.entries(favouritesMovies).reduce((output, movie) => {

//  }, {})


// console.log(movieGenres)